// Global calendar object
calendarDay = null;

/**
 * CalendarDay class
 */

class CalendarDay {
  constructor() {
    // Calender canvas
    this.calendarCanvas = document.getElementById("calendarCanvas");
    this.calendarContext = this.calendarCanvas.getContext('2d');
    this.width = this.calendarCanvas.width;
    this.height = this.calendarCanvas.height;

    // Event
    this.eventContainerWidth = 600;
    this.eventContainerHeight = 720;
    this.eventContainerPaddingLeft = 10;
    this.eventContainerPaddingRight = 10;
    this.eventPadding = 20;
    this.eventContainerOriginX = 115;
    this.eventContainerOriginY = 17;

    // Time
    this.timeContainerOriginX = 100;
    this.timeContainerOriginY = 20;
    this.timeContainerWidth = 150;
    this.timeContainerHeight = this.eventContainerHeight;

    // Grid values
    this.rowHeight = this.eventContainerHeight / 24;  // 30-minute intervals
    this.lineWidth = 1;

    /**
     * Data
     */

    // Array of event buckets, where each bucket contains
    // event. Multiple events per bucket for collision.
    this.events = [];
    this.maxCollisionDepth = []; // for each bucket
  }

  drawTimes() {
    // Start time of day
    var date = new Date();
    date.setHours(9,0,0);
    var time = (date.getHours() % 12 || 12) + ":" + ('0'+date.getMinutes()).slice(-2);

    // Declare origins locally for drawing
    var x = this.timeContainerOriginX,
        y = this.timeContainerOriginY;

    this.calendarContext.textAlign = 'right';

    for (var i = 0; i <= 24; i++) {
      this.calendarContext.font = '9pt Arial';
      this.calendarContext.fillStyle = '#9c9c9c';

      // Meridian
      if (i % 2 == 0) {
        this.calendarContext.fillText(date.getHours() >= 12 ? 'PM' : 'AM', x, y);
        this.calendarContext.font = 'Bold 11pt Arial';
        this.calendarContext.fillStyle = '#8b8b8b';
        x -= 25;
      }

      this.calendarContext.fillText(time, x, y);

      // Update for next iteration
      date = new Date(date.getTime() + 30 * 60000); // add 30 minutes
      time = (date.getHours() % 12 || 12) + ":" + ('0'+date.getMinutes()).slice(-2);
      x += i % 2 == 0 ? 25 : 0; // update origins
      y += this.rowHeight;
    }
  }

  clearEvents() {
    this.events = [];
    this.maxCollisionDepth = [];
    this.calendarContext.clearRect(this.eventContainerOriginX,
                                   this.eventContainerOriginY,
                                   this.eventContainerWidth + this.eventContainerPaddingLeft + this.eventContainerPaddingRight,
                                   this.eventContainerHeight);
  }

  drawEventsContainer() {
    // Clear events
    this.clearEvents();

    this.calendarContext.beginPath();
    this.calendarContext.fillStyle = "#ededed";
    this.calendarContext.fillRect(this.eventContainerOriginX,
                                  this.eventContainerOriginY,
                                  this.eventContainerWidth + this.eventContainerPaddingLeft + this.eventContainerPaddingRight,
                                  this.eventContainerHeight);
  }

  init() {

    /**
     * Draw calendar
     */

    // Draw time labels
    this.drawTimes();

    // Draw events container
    this.drawEventsContainer();
  }

  drawEvent(e, width) {
    this.calendarContext.textAlign = 'left';

    var offsetX = e.collisionDepth * width + this.lineWidth*2 || 0;

    // Rect
    this.calendarContext.beginPath();
    this.calendarContext.lineWidth = this.lineWidth;
    this.calendarContext.strokeStyle = "#dadada";
    this.calendarContext.rect(this.eventContainerOriginX + this.eventContainerPaddingLeft + offsetX,
                              this.eventContainerOriginY + e.start - 3,
                              width,
                              e.end - e.start);
    this.calendarContext.stroke();
    this.calendarContext.fillStyle = "#ffffff";
    this.calendarContext.fillRect(this.eventContainerOriginX + this.eventContainerPaddingLeft + offsetX + this.lineWidth,
                                  this.eventContainerOriginY + e.start + this.lineWidth - 3,
                                  width - this.lineWidth*2,
                                  e.end - e.start - this.lineWidth*2);

    // Left stroke
    this.calendarContext.beginPath();
    this.calendarContext.lineWidth = "5";
    this.calendarContext.strokeStyle = "#4eaf88";
    this.calendarContext.moveTo(this.eventContainerOriginX + this.eventContainerPaddingLeft + offsetX,
                                this.eventContainerOriginY + e.start - 3);
    this.calendarContext.lineTo(this.eventContainerOriginX + this.eventContainerPaddingLeft + offsetX,
                                this.eventContainerOriginY + e.end - 3);
    this.calendarContext.stroke();

    // Title
    this.calendarContext.font = '11pt Arial';
    this.calendarContext.fillStyle = '#71a690';
    this.calendarContext.fillText('Sample Item',
                                  this.eventContainerOriginX + this.eventContainerPaddingLeft + this.eventPadding + offsetX,
                                  this.eventContainerOriginY + e.start - 3 + this.eventPadding);

    // Location
    this.calendarContext.font = '9pt Arial';
    this.calendarContext.fillStyle = '#9c9c9c';
    this.calendarContext.fillText('Sample Location',
                                  this.eventContainerOriginX + this.eventContainerPaddingLeft + this.eventPadding + offsetX,
                                  this.eventContainerOriginY + e.start - 3 + this.eventPadding + 15);
  }

  drawEvents() {
    for (var i = 0; i < this.events.length; i++) {

      // Determine invariant width 'W'
      var W = this.eventContainerWidth / (this.maxCollisionDepth[i] + 1);

      for (var j = 0; j < this.events[i].length; j++) {
        this.drawEvent(this.events[i][j], W);
      }
    }
  }

  hash(e) {
    for (var i = 0; i < this.events.length; i++) {
      var collision = false;
      var collisionDepth = 0;
      for (var j = 0; j < this.events[i].length; j++) {
        // Check for collision
        if (Math.max(this.events[i][j].start, e.start) <= Math.min(this.events[i][j].end, e.end)) {
          collision = true;

          if (j == collisionDepth) {
            collisionDepth++;
          }
        }
      }

      // Store collisionDepth for this bucket
      this.maxCollisionDepth[i] = this.maxCollisionDepth[i] ? Math.max(this.maxCollisionDepth[i], collisionDepth) : collisionDepth;

      if (collision) {
        e.collisionDepth = collisionDepth || undefined;

        // Push event into bucket
        this.events[i].push(e);
        return;
      }
    }

    // No collisions, create new bucket for event
    this.events.push([e]);
  }

  loadEvents(events) {
    // Clear events
    this.drawEventsContainer();

    // Hash event data to handle collisions
    for (var i = 0; i < events.length; i++) {
      this.hash(events[i]);
    }

    // Draw events
    this.drawEvents();
  }
}

function initCalendar() {
  calendarDay = new CalendarDay();
  calendarDay.init();
}

function layOutDay(events) {
  calendarDay.loadEvents(events);
}
